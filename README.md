# f3db

f³db: fast flat file database

I am working on a super-simple DB engine, based on append-only flat text files. Features include full record history, network replication instead of syncing, and optional automatic record metadata (including ID, timestamps, user, client address). It's called f³db: fast flat file database, somewhat inspired by the original NoSQL database.

http://www.strozzi.it/cgi-bin/CSA/tw7/I/en_US/NoSQL/Home%20Page
